import 'package:calculator/bloc/calculator_bloc.dart';
import 'package:calculator/screens/caculator_screen.dart';
import 'package:calculator/services/calculator_history_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  runApp(CalculatorApp(sharedPreferences: sharedPreferences,));
}

class CalculatorApp extends StatelessWidget {
  const CalculatorApp({Key? key, required this.sharedPreferences}) : super(key: key);

  final SharedPreferences sharedPreferences;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Simple calculator',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.black54,
        backgroundColor: Colors.white,
        textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
            textStyle: MaterialStateProperty.all<TextStyle>(const TextStyle(fontSize: 20)),
            minimumSize: MaterialStateProperty.all<Size>(const Size.square(70))
          )
        ),
        textTheme: const TextTheme(
          bodyText2: TextStyle(fontSize: 30)
        )
      ),
      home: BlocProvider(
       create: (context) {
         return CalculatorBloc(calculatorHistoryService: CalculatorHistoryService(
           sharedPreferences: sharedPreferences
         ));
       },
        child: const CalculatorScreen(),
      )
    );
  }
}
