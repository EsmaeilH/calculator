
import 'package:calculator/bloc/calculator_event.dart';
import 'package:flutter/material.dart';
import 'package:calculator/bloc/calculator_bloc.dart';

class ButtonSectionWidget extends StatelessWidget {
  final CalculatorBloc calcBloc;

  const ButtonSectionWidget({required this.calcBloc, Key? key}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 7));},
                child: const Text("7")),
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 8));},
                child: const Text("8")),
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 9));},
                child: const Text("9")),
            TextButton(
                onPressed: (){calcBloc.add(OperandPressed(operator: '*'));},
                child: const Text("x")),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 4));},
                child: const Text("4")),
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 5));},
                child: const Text("5")),
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 6));},
                child: const Text("6")),
            TextButton(
                onPressed: (){calcBloc.add(OperandPressed(operator: '/'));},
                child: const Text("÷")),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 1));},
                child: const Text("1")),
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 2));},
                child: const Text("2")),
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 3));},
                child: const Text("3")),
            TextButton(
                onPressed: (){calcBloc.add(OperandPressed(operator: '+'));},
                child: const Text("+")),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
                onPressed: (){calcBloc.add(ClearPressed());},
                child: const Text("C")),
            TextButton(
                onPressed: (){calcBloc.add(NumberPressed(number: 0));},
                child: const Text("0")),
            TextButton(
                onPressed: (){calcBloc.add(EqualPressed());},
                child: const Text("=")),
            TextButton(
                onPressed: (){calcBloc.add(OperandPressed(operator: '-'));},
                child: const Text("−")),
          ],
        ),
      ],
    );
  }

}