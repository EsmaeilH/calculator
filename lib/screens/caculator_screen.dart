
import 'package:calculator/bloc/calculator_event.dart';
import 'package:calculator/bloc/calculator_state.dart';
import 'package:calculator/screens/widgets/button_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/calculator_bloc.dart';
import '../calculation_model.dart';

class CalculatorScreen extends StatefulWidget {
  const CalculatorScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CalculatorScreen();
  }
}

class _CalculatorScreen extends State<CalculatorScreen> {

  @override
  void initState() {
    context.read<CalculatorBloc>().add(FetchHistory());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Scaffold(
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: Container(
                    width: constraints.maxWidth,
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                    ),
                    child: BlocBuilder<CalculatorBloc,CalculatorState>(
                      builder: (context, state){
                        return Column(
                          children: [
                            Expanded(
                              child: ListView.builder(
                                itemCount: state.history.length,
                                reverse: true,
                                itemBuilder: (context, index) {
                                  return Text(state.history.reversed.toList()[index].toString());
                                }
                              ),
                            ),
                            const Divider(),
                            Text(_getDisplayText(state.calculationModel))
                          ],
                        );
                      },
                    )
                ),
              ),
              Flexible(
                flex: 4,
                fit: FlexFit.tight,
                child: Container(
                  width: constraints.maxWidth,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor
                  ),
                  child: ButtonSectionWidget(calcBloc: context.read<CalculatorBloc>())
                ),
              )
            ],
          ),
        );
      },
    );
  }

  String _getDisplayText(CalculationModel model) {
    if (model.result != null) {
      return '${model.result}';
    }

    if (model.secondOperand != null) {
      return '${model.firstOperand}${model.operator}${model.secondOperand}';
    }

    if (model.operator != null) {
      return '${model.firstOperand}${model.operator}';
    }

    if (model.firstOperand != null) {
      return '${model.firstOperand}';
    }

    return '${model.result ?? 0}';
  }

}
