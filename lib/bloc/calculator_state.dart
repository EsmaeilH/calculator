import 'package:calculator/calculation_model.dart';

abstract class CalculatorState {
  final CalculationModel calculationModel;
  final List<CalculationModel> history;

  const CalculatorState(
      {required this.calculationModel, required this.history});
}

class CalculatorInitial extends CalculatorState {
  CalculatorInitial()
      : super(calculationModel: const CalculationModel(), history: []);
}

class CalculatorChanged extends CalculatorState {
  const CalculatorChanged(
      {required CalculationModel calculationModel,
      required List<CalculationModel> history})
      : super(calculationModel: calculationModel, history: history);
}
