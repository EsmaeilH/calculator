
abstract class CalculatorEvent {}

class NumberPressed extends CalculatorEvent {
  final int number;

  NumberPressed({required this.number});
}

class OperandPressed extends CalculatorEvent {
  final String operator;

  OperandPressed({required this.operator});
}

class EqualPressed extends CalculatorEvent {}

class ClearPressed extends CalculatorEvent {}

class FetchHistory extends CalculatorEvent {}