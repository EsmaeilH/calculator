import 'package:calculator/bloc/calculator_state.dart';
import 'package:calculator/calculation_model.dart';
import 'package:calculator/services/calculator_history_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'calculator_event.dart';

class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  CalculatorBloc({required this.calculatorHistoryService})
      : super(CalculatorInitial()) {
    on<NumberPressed>((event, emit) => emit(_mapNumberPressedToState(event)));
    on<OperandPressed>(
        (event, emit) => emit(_mapOperatorPressedToState(event)));
    on<ClearPressed>((event, emit) => emit(_mapClearPressedToState()));
    on<EqualPressed>((event, emit) async => emit(await _mapCalculateResultToState()));
    on<FetchHistory>((event, emit) => emit(_mapHistoryToState()));
  }

  CalculatorHistoryService calculatorHistoryService;

  CalculatorState _mapNumberPressedToState(NumberPressed event) {
    final CalculationModel model = state.calculationModel;

    if (model.result != null) {
      final CalculationModel newModel =
          model.copyWith(firstOperand: event.number);

      return CalculatorChanged(
          calculationModel: newModel, history: List.of(state.history));
    }

    if (model.firstOperand == null) {
      final CalculationModel newModel =
          model.copyWith(firstOperand: event.number);

      return CalculatorChanged(
          calculationModel: newModel, history: List.of(state.history));
    }

    if (model.operator == null) {
      final CalculationModel newModel = model.copyWith(
        firstOperand: int.parse('${model.firstOperand}${event.number}'),
      );

      return CalculatorChanged(
          calculationModel: newModel, history: List.of(state.history));
    }

    if (model.secondOperand == null) {
      final CalculationModel newModel =
          model.copyWith(secondOperand: event.number);

      return CalculatorChanged(
          calculationModel: newModel, history: List.of(state.history));
    }

    return CalculatorChanged(
        calculationModel: model.copyWith(
            secondOperand: int.parse('${model.secondOperand}${event.number}')),
        history: List.of(state.history));
  }

  CalculatorState _mapOperatorPressedToState(OperandPressed event) {
    final List<String> allowedOperator = ['+', '-', '*', '/'];

    if (!allowedOperator.contains(event.operator)) {
      return state;
    }

    final CalculationModel model = state.calculationModel;

    final CalculationModel newModel = model.copyWith(
        firstOperand: model.firstOperand ?? 0, operator: event.operator);

    return CalculatorChanged(
        calculationModel: newModel, history: List.of(state.history));
  }

  Future<CalculatorState> _mapCalculateResultToState() async{
    final CalculationModel model = state.calculationModel;

    if (model.operator == null || model.secondOperand == null) {
      return state;
    }

    int result = 0;

    switch (model.operator) {
      case '+':
        result = model.firstOperand! + model.secondOperand!;
        break;
      case '-':
        result = model.firstOperand! - model.secondOperand!;
        break;
      case '*':
        result = model.firstOperand! * model.secondOperand!;
        break;
      case '/':
        if (model.secondOperand == 0) {
          result = 0;
        } else {
          result = model.firstOperand! ~/ model.secondOperand!;
        }
        break;
    }

    final CalculationModel newModel =
        CalculatorInitial().calculationModel.copyWith(
              firstOperand: result,
            );

    final resultModel = model.copyWith(result: newModel.firstOperand);

    if (await calculatorHistoryService.addEntry(resultModel)) {
      return CalculatorChanged(
        calculationModel: newModel,
        history: calculatorHistoryService.fetchAllEntries(),
      );
    }

    return CalculatorChanged(
        calculationModel: newModel, history: List.of(state.history));
  }

  CalculatorState _mapClearPressedToState() {
    CalculationModel resultModel =
        CalculatorInitial().calculationModel.copyWith();

    return CalculatorChanged(
        calculationModel: resultModel, history: List.of(state.history));
  }

  CalculatorState _mapHistoryToState() {
    return CalculatorChanged(calculationModel: state.calculationModel,
        history: calculatorHistoryService.fetchAllEntries()
    );
  }
}
