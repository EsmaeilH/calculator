import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import '../calculation_model.dart';

class CalculatorHistoryService {
  final SharedPreferences sharedPreferences;

  CalculatorHistoryService({required this.sharedPreferences});

  final String _sharedPreferenceKey = "history";

  List<CalculationModel> fetchAllEntries() {
    final List<CalculationModel> historyModel = <CalculationModel>[];

    if (!sharedPreferences.containsKey(_sharedPreferenceKey)) {
      return [];
    }

    List<dynamic> history = [];

    try {
      final String? storedValue =
          sharedPreferences.getString(_sharedPreferenceKey);
      if (storedValue == null) {
        return [];
      }
      history = jsonDecode(storedValue) as List<dynamic>;
    } on FormatException {
      sharedPreferences.remove(_sharedPreferenceKey);
    }

    for (final dynamic item in history) {
      historyModel.add(CalculationModel.fromJson(item as Map<String, dynamic>));
    }

    return historyModel;
  }

  Future<bool> addEntry(CalculationModel model) {
    final List<CalculationModel> allEntries = fetchAllEntries();
    allEntries.add(model);

    return sharedPreferences.setString(
        _sharedPreferenceKey, jsonEncode(allEntries));
  }
}
