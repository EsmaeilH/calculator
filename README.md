# Calculator

## About the app

This is a mobile application based on flutter framework.
it is a simple calculator with four major operations(add,sub,mul,div).

## Technologies

using bloc design pattern for state management
design responsive to work on different screens

## packages

flutter_bloc
